package com.company;

import java.util.Random;

public class Vector2D {

    private int _x1;
    private int _y1;

    public Vector2D() {
        _x1 = 0;
        _y1 = 0;
    }

    public Vector2D(int x, int y) {
        _x1 = x;
        _y1 = y;
    }
    //Длинна вектора
    public Double length() {
        return Math.sqrt(Math.pow(_x1, 2) + Math.pow(_y1, 2));
    }
    //Скалярное произведение векторов
    public Integer scolarMultiplication(Vector2D vector){
        return this.get_x1()* vector.get_x1()+this.get_y1()* vector.get_y1();
    }

    public Double scolarMultiplication(){
        return Math.pow(this.length(),2);
    }

    //Угол между векторами
    public Double angle(Vector2D vector){
        return (this.scolarMultiplication(vector))/(this.length()*vector.length());
    }
    //Сумма векторов
    public Vector2D sum(Vector2D vector){
        return new Vector2D(this.get_x1()+vector.get_x1(),this.get_y1()+vector.get_y1());
    }
    //Разность векторов
    public Vector2D difference(Vector2D vector){
        return new Vector2D(this.get_x1()-vector.get_x1(),this.get_y1()-vector.get_y1());
    }

    public static Vector2D[] generateArrVector(int size){
        Vector2D[] arr = new Vector2D[size];

        Random rng = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new Vector2D(rng.nextInt(), rng.nextInt());
        }

        return arr;
    }

    public Integer get_x1() {
        return _x1;
    }
    public Integer get_y1() {
        return _y1;
    }

    @Override
    public String toString() {
        return "x= " + _x1 + "\ty= " + _y1;
    }
}
