package com.company;

public class Main {

    public static void main(String[] args) {

        Vector3D vector1 = new Vector3D(-9,13,10);
        Vector3D vector2 = new Vector3D(5,5,5);

        System.out.println("Длинна вектора 1 = " + vector1.length());
        System.out.println("Длинна вектора 2 = " +vector2.length());

        System.out.println("Скольярное произведение вектора 1 с вектором 2 = " + vector1.scolarMultiplication(vector2));
        System.out.println("Косинус угла между вектором 1 и 2 = " + vector1.angle(vector2));
        System.out.println("Сумма вектора 1 и вектора 2 " + vector1.sum(vector2).toString());
        System.out.println("Разность вектора 1 и вектора 2 " + vector1.difference(vector2).toString());
        System.out.println("Векторное произведение векторов " + vector1.compos(vector2).toString());


        Vector3D[] arr = Vector3D.generateArrVector(5);

        for (Vector3D vector3D : arr) {
            System.out.println(vector3D.toString());
        }
    }
}
