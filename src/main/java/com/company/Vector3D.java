package com.company;
import java.util.Random;

public class Vector3D extends Vector2D {

    private int _z1;

    public Vector3D(int x, int y, int z) {
        super(x, y);
        _z1 = z;
    }

    public Vector3D() {
        super();
        _z1 = 0;
    }

    //Длинна вектора
    @Override
    public Double length() {
        return Math.sqrt(Math.pow(this.get_x1(), 2) + Math.pow(this.get_y1(), 2) + Math.pow(this.get_z1(), 2));
    }

    //Скалярное произведение
    public Integer scolarMultiplication(Vector3D vector) {
        return this.get_x1() * vector.get_x1() + this.get_y1() * vector.get_y1() + this.get_z1() * vector.get_z1();
    }
    
    @Override
    public Double scolarMultiplication(){
        return Math.pow(this.length(),2);
    }

    //Векторное произведение
    public Vector3D compos(Vector3D vector){
        return new Vector3D(
                this.get_y1()*vector.get_z1()-this.get_z1()*vector.get_y1(),
                this.get_z1()*vector.get_x1()-this.get_x1()*vector.get_z1(),
                this.get_x1()*vector.get_y1()-this.get_y1()*vector.get_x1()
        );
    }

    //Угол между векторами
    public Double angle(Vector3D vector) {
        return this.scolarMultiplication(vector) / (this.length() * vector.length());
    }

    //Сумма векторов
    public Vector3D sum(Vector3D vector) {
        return new Vector3D(
                this.get_x1() + vector.get_x1(),
                this.get_y1() + vector.get_y1(),
                this.get_z1() + vector.get_z1());
    }

    //Разность векторов
    public Vector3D difference(Vector3D vector) {
        return new Vector3D(
                this.get_x1() - vector.get_x1(),
                this.get_y1() - vector.get_y1(),
                this.get_z1() - vector.get_z1()
        );
    }

    //Генерация массива случайных векторов
    public static Vector3D[] generateArrVector(int size) {
        Vector3D[] arr = new Vector3D[size];

        Random rng = new Random();

        for (int i = 0; i < arr.length; i++) {
            arr[i] = new Vector3D(rng.nextInt(), rng.nextInt(), rng.nextInt());
        }

        return arr;
    }

    public int get_z1() {
        return _z1;
    }

    @Override
    public String toString() {
        return super.toString()+"\tz= " + _z1;
    }
}
